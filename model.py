import torch 
import torch.nn as nn
import torchvision.transforms.functional as TF
from axial_attention import AxialAttention


# attn = AxialAttention(
#     dim = 1,               # embedding dimension
#     dim_index = 1,         # where is the embedding dimension
#     dim_heads = 32,        # dimension of each head. defaults to dim // heads if not supplied
#     heads = 1,             # number of heads for multi-head attention
#     num_dimensions = 2,    # number of axial dimensions (images is 2, video is 3, or more)
#     sum_axial_out = True   # whether to sum the contributions of attention on each axis, or to run the input through them sequentially. defaults to true
# )

#  原图中蓝箭头:Conv 3 * 3 & ReLU
class DoubleConv(nn.Module): 
    def __init__(self, in_channels, out_channels):
        super(DoubleConv, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, 3, 1, 1, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(out_channels, out_channels, 3, 1, 1, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True),
        )
    
    def forward(self, x):
        return self.conv(x)




## ===================  Attention U-Net =================== ##
class AttUNET(nn.Module):
    def __init__(
        self, in_channels=3, out_channels=1, features=[64, 128, 256, 512]
    ):
        super(AttUNET, self).__init__()
        self.downs = nn.ModuleList()
        self.ups = nn.ModuleList()
        
        ## ===================  Attention module =================== ##

        self.attn = nn.ModuleList()

        ## ===================  Attention module =================== ##
        # 红箭头 最大值池化
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)

        # Down part of UNET
        for feature in features:
            # 将attention layers 加入到模型中
            self.attn.append(AxialAttention(
                dim = in_channels,               # embedding dimension
                dim_index = 1,         # where is the embedding dimension
                dim_heads = 4,        # dimension of each head. defaults to dim // heads if not supplied
                heads = 1,             # number of heads for multi-head attention
                num_dimensions = 2,    # number of axial dimensions (images is 2, video is 3, or more)
                sum_axial_out = True   # whether to sum the contributions of attention on each axis, or to run the input through them sequentially. defaults to true
            ))
            self.downs.append(DoubleConv(in_channels, feature))
            in_channels = feature

        # up part of UNET 将参数反过来 从最后一层开始
        for feature in reversed(features):
            self.ups.append(
                # 绿箭头 上采样
                nn.ConvTranspose2d(
                    feature*2, feature, kernel_size=2, stride=2,
                )
            )
            self.ups.append(DoubleConv(feature*2, feature))


        self.bottleneck = DoubleConv(features[-1], features[-1]*2)
        self.final_conv = nn.Conv2d(features[0], out_channels, kernel_size=1)

    def forward(self, x):

        skip_connections = []
        count = 0

        for down in self.downs:
            # 下采样过程中将 中间结果 记录下来
            # add attention module
            if count == 2:
                x = self.attn[count](x)
            x = down(x)
            skip_connections.append(x)
            x = self.pool(x)
            count += 1

        x = self.bottleneck(x)
        skip_connections = skip_connections[::-1] # reverse that list 即取原列表的倒序
        # 因为ups列表里间隔存了ConvTranspose2d和DoubleConv 所以索引分奇偶
        # 这里只取偶数则对应 ConvTranspose2d
        for idx in range(0, len(self.ups), 2):
            x = self.ups[idx](x)
            skip_connection = skip_connections[idx//2]
            # resize 
            if x.shape != skip_connection.shape:
                x = TF.resize(x, size=skip_connection.shape[2:])

            concat_skip = torch.cat((skip_connection, x), dim=1)
            x = self.ups[idx+1](concat_skip)



        return self.final_conv(x)

## ==================  UNET ================== ##
class UNET(nn.Module):
    def __init__(
        self, in_channels=3, out_channels=1, features=[64, 128, 256, 512]
    ):
        super(UNET, self).__init__()
        self.downs = nn.ModuleList()
        self.ups = nn.ModuleList()
        # 红箭头 最大值池化
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)

        # Down part of UNET
        for feature in features:
            self.downs.append(DoubleConv(in_channels, feature))
            in_channels = feature

        # up part of UNET 将参数反过来
        for feature in reversed(features):
            self.ups.append(
                # 绿箭头 上采样
                nn.ConvTranspose2d(
                    feature*2, feature, kernel_size=2, stride=2,
                )
            )
            self.ups.append(DoubleConv(feature*2, feature))

        self.bottleneck = DoubleConv(features[-1], features[-1]*2)
        self.final_conv = nn.Conv2d(features[0], out_channels, kernel_size=1)

    def forward(self, x):

        skip_connections = []
        # 下采样过程
        for down in self.downs:
            # 下采样过程中将 中间结果 记录下来
            x = down(x)
            skip_connections.append(x)
            x = self.pool(x)


        x = self.bottleneck(x)
        skip_connections = skip_connections[::-1] # reverse that list 即取原列表的倒序
        # 因为ups列表里间隔存了ConvTranspose2d和DoubleConv 所以索引分奇偶
        # 这里只取偶数则对应 ConvTranspose2d
        for idx in range(0, len(self.ups), 2):
            x = self.ups[idx](x)
            skip_connection = skip_connections[idx//2]
            # resize 
            if x.shape != skip_connection.shape:
                x = TF.resize(x, size=skip_connection.shape[2:])

            concat_skip = torch.cat((skip_connection, x), dim=1)
            x = self.ups[idx+1](concat_skip)

        return self.final_conv(x)

class AttUNETa(nn.Module):
    '''
    变结构的AttUNET
    在解码的时候加入attention
    '''
    def __init__(
        self, in_channels=3, out_channels=1, features=[64, 128, 256, 512]
    ):
        super(AttUNETa, self).__init__()
        self.downs = nn.ModuleList()
        self.ups = nn.ModuleList()
        
        ## ===================  Attention module =================== ##

        self.attn = nn.ModuleList()

        ## ===================  Attention module =================== ##
        # 红箭头 最大值池化
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)

        # Down part of UNET
        for feature in features:
            self.downs.append(DoubleConv(in_channels, feature))
            in_channels = feature

        # up part of UNET 将参数反过来 从最后一层开始
        for feature in reversed(features):
            self.ups.append(
                # 绿箭头 上采样
                nn.ConvTranspose2d(
                    feature*2, feature, kernel_size=2, stride=2,
                )
            )
            self.ups.append(DoubleConv(feature*2, feature))
             # 将attention layers 加入到模型中
            self.attn.append(AxialAttention(
                dim = feature*2,               # embedding dimension
                dim_index = 1,         # where is the embedding dimension
                dim_heads = 4,        # dimension of each head. defaults to dim // heads if not supplied
                heads = 1,             # number of heads for multi-head attention
                num_dimensions = 2,    # number of axial dimensions (images is 2, video is 3, or more)
                sum_axial_out = True   # whether to sum the contributions of attention on each axis, or to run the input through them sequentially. defaults to true
            ))


        self.bottleneck = DoubleConv(features[-1], features[-1]*2)
        self.final_conv = nn.Conv2d(features[0], out_channels, kernel_size=1)

    def forward(self, x):

        skip_connections = []
        count = 0

        for down in self.downs:
            # 下采样过程中将 中间结果 记录下来
            # # add attention module
            # if count == 2:
            #     x = self.attn[count](x)
            x = down(x)
            skip_connections.append(x)
            x = self.pool(x)
            count += 1

        x = self.bottleneck(x)
        skip_connections = skip_connections[::-1] # reverse that list 即取原列表的倒序
        # 因为ups列表里间隔存了ConvTranspose2d 和 DoubleConv 所以索引分奇偶
        # 这里只取偶数则对应 ConvTranspose2d
        for idx in range(0, len(self.ups), 2):
            x = self.ups[idx](x)
            skip_connection = skip_connections[idx//2]
            # resize 
            if x.shape != skip_connection.shape:
                x = TF.resize(x, size=skip_connection.shape[2:])

            concat_skip = torch.cat((skip_connection, x), dim=1)
            if idx + 1 == 4:
                x = self.attn[idx//2](concat_skip)
            x = self.ups[idx+1](concat_skip)



        return self.final_conv(x)


class AttUNETb(nn.Module):
    '''
    变结构的AttUNET
    在解码的时候加入attention
    '''
    def __init__(
        self, in_channels=3, out_channels=1, features=[64, 128, 256, 512]
    ):
        super(AttUNETb, self).__init__()
        self.downs = nn.ModuleList()
        self.ups = nn.ModuleList()
        
        ## ===================  Attention module =================== ##

        self.attn_down = nn.ModuleList()
        self.attn_up = nn.ModuleList()


        ## ===================  Attention module =================== ##
        # 红箭头 最大值池化
        self.pool = nn.MaxPool2d(kernel_size=2, stride=2)

        # Down part of UNET
        for feature in features:
            # 将attention layers 加入到模型中
            self.attn_down.append(AxialAttention(
                dim = in_channels,               # embedding dimension
                dim_index = 1,         # where is the embedding dimension
                dim_heads = 4,        # dimension of each head. defaults to dim // heads if not supplied
                heads = 1,             # number of heads for multi-head attention
                num_dimensions = 2,    # number of axial dimensions (images is 2, video is 3, or more)
                sum_axial_out = True   # whether to sum the contributions of attention on each axis, or to run the input through them sequentially. defaults to true
            ))
            self.downs.append(DoubleConv(in_channels, feature))
            in_channels = feature

        # up part of UNET 将参数反过来 从最后一层开始
        for feature in reversed(features):
            self.ups.append(
                # 绿箭头 上采样
                nn.ConvTranspose2d(
                    feature*2, feature, kernel_size=2, stride=2,
                )
            )
            self.ups.append(DoubleConv(feature*2, feature))
             # 将attention layers 加入到模型中
            self.attn_up.append(AxialAttention(
                dim = feature*2,               # embedding dimension
                dim_index = 1,         # where is the embedding dimension
                dim_heads = 4,        # dimension of each head. defaults to dim // heads if not supplied
                heads = 1,             # number of heads for multi-head attention
                num_dimensions = 2,    # number of axial dimensions (images is 2, video is 3, or more)
                sum_axial_out = True   # whether to sum the contributions of attention on each axis, or to run the input through them sequentially. defaults to true
            ))


        self.bottleneck = DoubleConv(features[-1], features[-1]*2)
        self.final_conv = nn.Conv2d(features[0], out_channels, kernel_size=1)

    def forward(self, x):

        skip_connections = []
        skip_attn = []
        count = 0

        for down in self.downs:
            # 下采样过程中将 中间结果 记录下来
            # add attention module
            if count == 2:
                x = self.attn_down[count](x)
                skip_attn.append(x) # 保存attention的结果
            x = down(x)
            skip_connections.append(x)

            x = self.pool(x)
            count += 1

        x = self.bottleneck(x)
        skip_connections = skip_connections[::-1] # reverse that list 即取原列表的倒序
        # 因为ups列表里间隔存了ConvTranspose2d 和 DoubleConv 所以索引分奇偶
        # 这里只取偶数则对应 ConvTranspose2d
        for idx in range(0, len(self.ups), 2):
            x = self.ups[idx](x)
            skip_connection = skip_connections[idx//2]
            # resize 
            if x.shape != skip_connection.shape:
                x = TF.resize(x, size=skip_connection.shape[2:])

            concat_skip = torch.cat((skip_connection, x), dim=1)
            if idx + 1 == 4:
                concat_skip = torch.cat((skip_attn[0], concat_skip), dim=1) # 将attention的结果和skip connection的结果拼接
                x = self.attn_up[idx//2](concat_skip)
            x = self.ups[idx+1](concat_skip)



        return self.final_conv(x)





# test model
if __name__ == "__main__":
    x = torch.randn((3, 1, 161, 161))
    # model = UNET(in_channels=1, out_channels=1)
    # preds = model(x)
    # print(preds.shape)
    # print(x.shape)
    # print(preds[0].shape)
    # print(x[0].shape)

    # test attUNET
    # model = AttUNET(in_channels=1, out_channels=1)
    # preds = model(x)
    # print(model)
    # print(preds.shape)

    # test attUNETa
    # x = torch.randn((3, 1, 161, 161))
    # model = AttUNETa(in_channels=1, out_channels=1)
    # preds = model(x)
    # print(model)
    # print(preds.shape)

    # test attUNETb
    x = torch.randn((3, 1, 161, 161))
    model = AttUNETb(in_channels=1, out_channels=1)
    preds = model(x)
    print(model)
    print(preds.shape)



