import torch
import torchvision
from dataset import MRIset2D
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter



def save_checkpoint(state, filename):
    print("=> Saving checkpoint")
    torch.save(state, filename)

def load_checkpoint(checkpoint, model):
    print("=> Loading checkpoint")
    model.load_state_dict(checkpoint["state_dict"])

def get_loaders(
    dir,
    batch_size,
    train_transform,
    val_transform,
    num_workers=2,
    pin_memory = True,
):  
    train_ds = MRIset2D(dir, mode="train", transform=train_transform)
    val_ds = MRIset2D(dir, mode="test", transform=val_transform)

    train_loader = DataLoader(
        train_ds,
        batch_size=batch_size,
        num_workers=num_workers,
        pin_memory=pin_memory,
        shuffle=True,
    )
    val_loader = DataLoader(
        val_ds,
        batch_size=batch_size,
        num_workers=num_workers,
        pin_memory=pin_memory,
        shuffle=False,
    )

    return train_loader, val_loader

def check_accuracy(loader, model, device, global_step):
    num_correct = 0
    num_pixels = 0
    dice_score = 0
    model.eval()

    with torch.no_grad():
        for x, y in loader:
            x = x.to(device)
            y = y.to(device)
            preds = torch.sigmoid(model(x))
            preds = (preds > 0.5).float()
            num_correct += (preds == y).sum()
            num_pixels += torch.numel(preds)

            dice_score += (2 * (preds * y).sum()) /(
                (preds + y).sum() + 1e-8
            )

    print(
        f"Got {num_correct}/{num_pixels} with acc {num_correct/num_pixels * 100 :.2f}"
    )
    print(f"Dice score: {dice_score/len(loader)}")

    # write to tensorboard
    writer = SummaryWriter("runs/fcn")
    writer.add_scalar("val/accuracy", num_correct/num_pixels, global_step=global_step)
    writer.add_scalar("val/dice_score", dice_score/len(loader), global_step=global_step)

    model.train()

def save_predictions_as_imgs(loader, model, device, folder="saved_images/" ):
    model.eval()
    for idx, (x, y) in enumerate(loader):
        x = x.to(device=device)
        with torch.no_grad():
            preds = torch.sigmoid(model(x))
            preds = (preds > 0.5).float()
        torchvision.utils.save_image(
            preds, f"{folder}/pred_{idx}.png"
        )
        break

# Intersection over Union (IoU) metric for semantic segmentation in binary case
# 计算交并比 交集/并集 （二值）
def iou(preds, target):
    # preds and target are torch tensors
    # preds = (preds > 0.5).float()
    intersection = (preds * target).sum()
    union = preds.sum() + target.sum() - intersection
    return (intersection + 1e-8) / (union + 1e-8)

# Dice coefficient for semantic segmentation in binary case
# 计算dice系数
def dice_coeff(preds, target):
    # preds and target are torch tensors
    # preds = (preds > 0.5).float()
    intersection = (preds * target).sum()
    return (2. * intersection + 1e-8) / (preds.sum() + target.sum() + 1e-8)

# pixel accuracy for semantic segmentation in binary case
# 计算像素准确率
def pixel_acc(preds, target):
    # preds and target are torch tensors
    # preds = (preds > 0.5).float()
    correct = (preds == target).sum()
    return (correct + 1e-8) / (preds.numel() + 1e-8)

# Get metrics for semantic segmentation in binary case (IoU, Dice, pixel accuracy)
# 计算交并比，dice系数，像素准确率
def get_metrics(preds, target):
    '''
    preds and target are torch tensors
    preds = (preds > 0.5).float()

    return {"iou": iou_score, "dice": dice_score, "pixel_acc": pixel_accuracy}    
    '''
    # preds and target are torch tensors
    # preds = (preds > 0.5).float()
    iou_score = iou(preds, target)
    dice_score = dice_coeff(preds, target)
    pixel_accuracy = pixel_acc(preds, target)
    return {"iou": iou_score, "dice": dice_score, "pixel_acc": pixel_accuracy}

# Using model to predict input image
# 使用模型预测输入图像
def predict_image(model, image):
    # image is a torch tensor
    # image = image.unsqueeze(0)
    preds = torch.sigmoid(model(image))
    preds = (preds > 0.5).float()
    return preds

# Using model to predict input image and get metrics
# 使用模型预测输入图像并计算交并比，dice系数，像素准确率
def predict_image_metrics(model, image, target):
    # image and target are torch tensors
    # image = image.unsqueeze(0)
    preds = torch.sigmoid(model(image))
    preds = (preds > 0.5).float()
    metrics = get_metrics(preds, target)
    return preds, metrics


# tensorboard writer add model structure
# tensorboard写入模型结构
def add_model_structure(model, image):
    writer = SummaryWriter("runs/fcn")
    writer.add_graph(model, image)








