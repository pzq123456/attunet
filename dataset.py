import numpy as np
import os
import SimpleITK as sitk
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

class MRIset2D(Dataset):
    '''The dataset for 2D MRI data'''
    def __init__(self, data_path, mode="train",transform=None,slice_num=60):

        self.data_path = data_path
        self.mode = mode
        self.namelist = []
        self.data_list = []
        self.label_list = []
        self.slice_num = slice_num
        self.transform = transform

        # get the name list"data/train.txt" or "data/test.txt"
        with open(os.path.join(data_path, mode + ".txt"), "r") as f:
            self.namelist = f.readlines()
        
        self.namelist = [x.strip() for x in self.namelist]
        # get the data list and label list
        for name in self.namelist: 
            self.data_list.append(os.path.join(data_path, mode, name + ".nii.gz"))
            self.label_list.append(os.path.join(data_path, "mask", name + ".segmentation_masks.mhd"))

    def __len__(self):
        return len(self.data_list)
    
    def __getitem__(self, idx):

        img = sitk.ReadImage(self.data_list[idx])
        img = sitk.GetArrayFromImage(img)
        img = img.astype(np.float32)
        

        seg = sitk.ReadImage(self.label_list[idx])
        seg = sitk.GetArrayFromImage(seg)
        seg = seg.astype(np.float32)
        

        seg[seg != 2] = 0
        seg[seg == 2] = 1

        image = img[:, :, self.slice_num]
        mask = seg[:, :, self.slice_num]

        # add the channel dimension
        image = np.expand_dims(image, axis=0)
        mask = np.expand_dims(mask, axis=0)

        return image, mask
    


    