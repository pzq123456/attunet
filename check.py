from model import UNET, AttUNET, AttUNETa, AttUNETb
from utils import (
    load_checkpoint,
    get_loaders,
    get_metrics,
)
import torch
import albumentations as A
from albumentations.pytorch import ToTensorV2
from torch.utils.tensorboard import SummaryWriter
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.patches as mpatches
import tqdm 

# 超参数 Hyperparametars etc.
LEARNING_RATE = 1e-4
device = "cuda" if torch.cuda.is_available() else "mps" if torch.backends.mps.is_available() else "cpu"


IMAGE_HEIGHT = 256 # 1280 originally
IMAGE_WIDTH = 256 # 1918 originally 

BATCH_SIZE = 4
NUM_WORKERS = 4

PIN_MEMORY = True
LOAD_MODEL = True
DIR = "data/"

def stretch(x):
    max = x.max()
    min = x.min()
    # print("normalization:{},{}".format(max, min))
    x = (x - min) / (max - min)
    # stretch by the function : y = x^(1/2)
    x = x ** 0.5
    return x ** 0.5


# main function load the latest checkpoint and check the accuracy save images
def main(path=None,model=None):
    train_tansforms = A.Compose(
        [
            A.Resize(height=IMAGE_HEIGHT, width=IMAGE_WIDTH),
            A.Rotate(limit=35, p=1.0),
            A.HorizontalFlip(p=0.5),
            A.Normalize(
                mean=[0.0, 0.0, 0.0],
                std=[1.0, 1.0, 1.0],
                max_pixel_value=255.0,
            ),
            ToTensorV2(),
        ],
    )

    val_transforms = A.Compose(
        [
            A.Resize(height=IMAGE_HEIGHT, width=IMAGE_WIDTH),
            A.Normalize(
                mean=[0.0, 0.0, 0.0],
                std=[1.0, 1.0, 1.0],
                max_pixel_value=255.0,
            ),
            ToTensorV2(),
        ],
    )

    _ , val_loader = get_loaders(
        DIR,
        BATCH_SIZE,
        train_tansforms,
        val_transforms,
        NUM_WORKERS,
        PIN_MEMORY,
    )



    # load parameters
    if path != None:
        load_checkpoint(torch.load(path), model)
        print("Checkpoint loaded")
        general_metrics = {"iou": 0, "dice": 0, "pixel_acc": 0} # general metrics
        print("Start checking {} images for {} model".format(len(val_loader), model.__class__.__name__))
        # using tqdm
        for idx, (x, y) in enumerate(tqdm.tqdm(val_loader)):
            # get label and prediction
            x = x.to(device=device)
            y = y.to(device=device)
            with torch.no_grad():
                # predict_image_metrics(model, x, y)
                preds = model(x)
                preds = (preds > 0.5).float()
                # get metrics
                metrics = get_metrics(preds, y)
                # add metrics
                general_metrics["iou"] += metrics["iou"]
                general_metrics["dice"] += metrics["dice"]
                general_metrics["pixel_acc"] += metrics["pixel_acc"]
                # save image
                if idx % 1 == 0:
                    # print("save image {}".format(idx))
                    # square normalization to [0,255]

                    x = stretch(x)

                    # compare the label and prediction and label the difference
                    # label the pixel TP, FP, FN, TN with 1,2,3,4 in one image
                    TP = torch.zeros_like(x)
                    FP = torch.zeros_like(x)
                    FN = torch.zeros_like(x)
                    TN = torch.zeros_like(x)
                    diff = torch.zeros_like(x)

                    # label the pixel TP, FP, FN, TN with 1,2,3,4 in one image
                    TP[(preds == 1) & (y == 1)] = 0 # green
                    FP[(preds == 1) & (y == 0)] = 1 # red
                    FN[(preds == 0) & (y == 1)] = 2 # blue
                    TN[(preds == 0) & (y == 0)] = 3 # black
                    diff = TP + FP + FN + TN

                    

                    # plot the difference

                    plt.figure(figsize=(20, 20))

                    plt.subplot(2, 4, 1)
                    plt.imshow(x[0].cpu().permute(1, 2, 0))
                    plt.title("Input")
                    plt.subplot(2, 4, 2)
                    cmap = mpl.colors.ListedColormap(['green', 'red', 'blue', 'black'])
                    plt.imshow(diff[0].cpu().permute(1, 2, 0), cmap=cmap)
                    plt.title("Difference")
                    # plot legend for the difference class in horizontal
                    green_patch = mpatches.Patch(color='green', label='TP')
                    red_patch = mpatches.Patch(color='red', label='FP')
                    blue_patch = mpatches.Patch(color='blue', label='FN')
                    black_patch = mpatches.Patch(color='black', label='TN')
                    plt.legend(handles=[green_patch, red_patch, blue_patch, black_patch], loc='upper center',
                                bbox_to_anchor=(0.5, -0.05), ncol=4)
                    

                    plt.subplot(2, 4, 3)
                    plt.imshow(x[1].cpu().permute(1, 2, 0))
                    plt.title("Input")
                    plt.subplot(2, 4, 4)
                    cmap = mpl.colors.ListedColormap(['green', 'red', 'blue', 'black'])
                    plt.imshow(diff[1].cpu().permute(1, 2, 0), cmap=cmap)
                    plt.title("Difference")
                    # plot legend for the difference class in horizontal
                    green_patch = mpatches.Patch(color='green', label='TP')
                    red_patch = mpatches.Patch(color='red', label='FP')
                    blue_patch = mpatches.Patch(color='blue', label='FN')
                    black_patch = mpatches.Patch(color='black', label='TN')
                    plt.legend(handles=[green_patch, red_patch, blue_patch, black_patch], loc='upper center',
                                bbox_to_anchor=(0.5, -0.05), ncol=4)

                    if len(x) == 2:
                        # UNET, AttUNET
                        if(model.__class__.__name__ == "UNET"):
                            plt.savefig("imgs/Unet/{}.png".format(idx))
                        elif(model.__class__.__name__ == "AttUNET"):
                            plt.savefig("imgs/attUnet/{}.png".format(idx))
                        elif(model.__class__.__name__ == "AttUNETa"):
                            plt.savefig("imgs/attUneta/{}.png".format(idx))
                        elif(model.__class__.__name__ == "AttUNETb"):
                            plt.savefig("imgs/attUnetb/{}.png".format(idx))
                        else :
                            print("model name error!")
                            return
                        plt.close()
                        # out the loop
                        break

                    plt.subplot(2, 4, 5)
                    plt.imshow(x[2].cpu().permute(1, 2, 0))
                    plt.title("Input")
                    plt.subplot(2, 4, 6)
                    cmap = mpl.colors.ListedColormap(['green', 'red', 'blue', 'black'])
                    plt.imshow(diff[2].cpu().permute(1, 2, 0), cmap=cmap)
                    plt.title("Difference")
                    # plot legend for the difference class in horizontal
                    green_patch = mpatches.Patch(color='green', label='TP')
                    red_patch = mpatches.Patch(color='red', label='FP')
                    blue_patch = mpatches.Patch(color='blue', label='FN')
                    black_patch = mpatches.Patch(color='black', label='TN')
                    plt.legend(handles=[green_patch, red_patch, blue_patch, black_patch], loc='upper center',
                                bbox_to_anchor=(0.5, -0.05), ncol=4)

                    plt.subplot(2, 4, 7)
                    plt.imshow(x[3].cpu().permute(1, 2, 0))
                    plt.title("Input")
                    plt.subplot(2, 4, 8)
                    cmap = mpl.colors.ListedColormap(['green', 'red', 'blue', 'black'])
                    plt.imshow(diff[3].cpu().permute(1, 2, 0), cmap=cmap)
                    plt.title("Difference")
                    # plot legend for the difference class in horizontal
                    green_patch = mpatches.Patch(color='green', label='TP')
                    red_patch = mpatches.Patch(color='red', label='FP')
                    blue_patch = mpatches.Patch(color='blue', label='FN')
                    black_patch = mpatches.Patch(color='black', label='TN')
                    plt.legend(handles=[green_patch, red_patch, blue_patch, black_patch], loc='upper center',
                                bbox_to_anchor=(0.5, -0.05), ncol=4)
                    # UNET, AttUNET
                    if(model.__class__.__name__ == "UNET"):
                        plt.savefig("imgs/Unet/{}.png".format(idx))
                    elif(model.__class__.__name__ == "AttUNET"):
                        plt.savefig("imgs/attUnet/{}.png".format(idx))
                    elif(model.__class__.__name__ == "AttUNETa"):
                        plt.savefig("imgs/attUneta/{}.png".format(idx))
                    elif(model.__class__.__name__ == "AttUNETb"):
                        plt.savefig("imgs/attUnetb/{}.png".format(idx))
                    else :
                        print("model name error!")
                        return
                    plt.close()

        # get the average metrics
        general_metrics["iou"] /= len(val_loader)
        general_metrics["dice"] /= len(val_loader)
        general_metrics["pixel_acc"] /= len(val_loader)
        print("iou: {:.3f}, dice: {:.3f}, pixel_acc: {:.3f}".format(general_metrics["iou"], general_metrics["dice"], general_metrics["pixel_acc"]))
    else:
        print("No checkpoint found!")

if __name__ == "__main__":
    # model = UNET(in_channels=1, out_channels=1).to(device)
    # # model = AttUNET(in_channels=1, out_channels=1).to(device)

    # x = torch.randn((1, 1, 256, 256)).to(device)
    # with SummaryWriter(comment='UNET') as w:
    #     w.add_graph(model, x)
    # print("write the model structure")


    # model = AttUNET(in_channels=1, out_channels=1).to(device)
    # main("checkpoints/attUnet/my_checkpoint_260.pth.tar",model)

    # model1 = UNET(in_channels=1, out_channels=1).to(device)
    # model = AttUNETb(in_channels=1, out_channels=1).to(device)
    model = AttUNETa(in_channels=1, out_channels=1).to(device)
    main("checkpoints/attUneta/my_checkpoint_190.pth.tar",model)


