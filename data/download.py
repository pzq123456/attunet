
# > - https://gitlab.com/pzq123456/train/-/raw/master/train/9002817.nii.gz?inline=false
# > - https://gitlab.com/pzq123456/test/-/raw/master/test/9002817.nii.gz?inline=false
# - 思路：
#   - 从 `test.txt` 和 `train.txt` 中读取文件名（这两个txt文件就是我们的元数据）
#   - 根据上述地址生成下载地址
#   - 下载并保存到对应文件夹下

import os
import requests
import shutil
import time
import sys
import argparse

def download(data_path, mode):
    # get the name list"data/train.txt" or "data/test.txt"
    with open(os.path.join(data_path, mode + ".txt"), "r") as f:
        namelist = f.readlines()
    
    namelist = [x.strip() for x in namelist]
    # get the data list and label list
    for name in namelist: 
        data_url = "https://gitlab.com/pzq123456/" + mode + "/-/raw/master/" + mode + "/" + name + ".nii.gz?inline=false"
        label_url = "https://gitlab.com/pzq123456/" + mode + "/-/raw/master/" + mode + "/" + name + ".segmentation_masks.mhd?inline=false"
        data_save_path = os.path.join(data_path, mode, name + ".nii.gz")
        label_save_path = os.path.join(data_path, "mask", name + ".segmentation_masks.mhd")
        print("Downloading " + name + ".nii.gz")
        download_file(data_url, data_save_path)
        print("Downloading " + name + ".segmentation_masks.mhd")
        download_file(label_url, label_save_path)
        print("Downloaded " + name + ".nii.gz and " + name + ".segmentation_masks.mhd")


def download_file(url, save_path):
    # NOTE the stream=True parameter below
    with requests.get(url, stream=True) as r:
        with open(save_path, 'wb') as f:
            shutil.copyfileobj(r.raw, f)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Download data")
    parser.add_argument('--data_path', type=str, default="data", help="The path to save data")
    args = parser.parse_args()
    download(args.data_path, "train")
    download(args.data_path, "test")
