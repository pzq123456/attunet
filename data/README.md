# 项目数据集制作教程
## 0. 先决条件
- 本教程假设你已经安装了 `python3` 和 `pip`。
- 本教程假设你已经安装了 `git`。
## 1. 下载 `mask` 数据集
> - [下载地址](https://www.kaggle.com/datasets/panzhiqing666/oia-mask/download?datasetVersionNumber=1)

- 从上述地址下载到压缩包，并解压到 `data` 目录下，得到 `segmentation_mask` 目录，目录结构如下：
```
data/
    segmentation_mask/
    test.txt
    train.txt
```
- 方便起见，我们将 `segmentation_mask` 目录重命名为 `mask`。

## 2. 下载 `test` `train` 数据集
> - 直接从 gitlab 上下载压缩包，可能会失败，因为文件太大
> - 我们改用脚本，单个文件下载并保存在对应文件夹下
> - https://gitlab.com/pzq123456/train/-/raw/master/train/9002817.nii.gz?inline=false
> - https://gitlab.com/pzq123456/test/-/raw/master/test/9002817.nii.gz?inline=false
- 思路：
  - 从 `test.txt` 和 `train.txt` 中读取文件名（这两个txt文件就是我们的元数据）
  - 根据上述地址生成下载地址
  - 下载并保存到对应文件夹下
-  下载脚本我已写好，见 `download.py`，运行即可
    ``` bash
    python download.py
    ```