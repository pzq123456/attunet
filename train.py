import torch
import albumentations as A
from albumentations.pytorch import ToTensorV2
from tqdm import tqdm
import torch.nn as nn
import torch.optim as optim 
from model import UNET, AttUNET, AttUNETa, AttUNETb
from utils import (
    load_checkpoint,
    save_checkpoint,
    get_loaders,
    check_accuracy,
    save_predictions_as_imgs,
)
from torch.utils.tensorboard import SummaryWriter

# 超参数 Hyperparametars etc.
LEARNING_RATE = 1e-4
device = "cuda" if torch.cuda.is_available() else "mps" if torch.backends.mps.is_available() else "cpu"
BATCH_SIZE = 16
NUM_EPOCHS = 500
NUM_WORKERS = 4

IMAGE_HEIGHT = 256 # 384 originally
IMAGE_WIDTH = 256 # 384 originally

PIN_MEMORY = True
LOAD_MODEL = False
DIR = "data/"

def train_fn(loader, model, optimizer, loss_fn, scaler):

    loop = tqdm(loader)

    for batch_idx, (data, targets) in enumerate(loop):
        data = data.to(device=device)
        targets = targets.to(device=device)
        # forward
        with torch.cuda.amp.autocast():
            predictions = model(data)
            loss = loss_fn(predictions, targets)

        # backward
        optimizer.zero_grad()
        scaler.scale(loss).backward()
        scaler.step(optimizer)
        scaler.update()

        # update tqdm loop
        loop.set_postfix(loss=loss.item())




def main(CKPT_PATH=None,model=None):
    train_tansforms = A.Compose(
        [
            A.Resize(height=IMAGE_HEIGHT, width=IMAGE_WIDTH),
            A.Rotate(limit=35, p=1.0),
            A.HorizontalFlip(p=0.5),
            A.Normalize(),
            ToTensorV2(),
        ],
    )

    val_transforms = A.Compose(
        [
            A.Resize(height=IMAGE_HEIGHT, width=IMAGE_WIDTH),
            A.Normalize(),
            ToTensorV2(),
        ],
    )

    loss_fn = nn.BCEWithLogitsLoss()
    optimizer = optim.Adam(model.parameters(), lr=LEARNING_RATE)

    train_loader, val_loader = get_loaders(
        DIR,
        BATCH_SIZE,
        train_tansforms,
        val_transforms,
        NUM_WORKERS,
        PIN_MEMORY,
    )

    scaler = torch.cuda.amp.grad_scaler.GradScaler()
    
    if CKPT_PATH is not None:
        # my_checkpoint_0.pth.tar -> 0
        load_checkpoint(torch.load(CKPT_PATH), model)
        current_epoch = int(CKPT_PATH.split("_")[-1][:-8])
        print(f"Checkpoint loaded: {CKPT_PATH}")
        print(" Checkpoint loaded successfully. Start training from epoch {}.".format(current_epoch))
        for epoch in range(current_epoch + 1, NUM_EPOCHS):
            train_fn(train_loader, model, optimizer, loss_fn, scaler)
            # save model
            checkpoint = {
                "state_dict": model.state_dict(),
                "optimizer": optimizer.state_dict(),
            }
            if epoch % 5 == 0:
                save_checkpoint(checkpoint,f"my_checkpoint_{epoch}.pth.tar")
                print("successfully saved {}.".format(f"my_checkpoint_{epoch}.pth.tar"))
                # check accuracy
                check_accuracy(val_loader, model, device, epoch)
                # print some examples to a folder
            if epoch % 10 == 0:
                save_predictions_as_imgs(
                    val_loader, model, device, folder="saved_images/"
                )

    else :
        print("No checkpoint found. Training from scratch.")  
        for epoch in range(NUM_EPOCHS):
            train_fn(train_loader, model, optimizer, loss_fn, scaler)
            # save model
            checkpoint = {
                "state_dict": model.state_dict(),
                "optimizer": optimizer.state_dict(),
            }
            if epoch % 5 == 0:
                print("Epoch {} finished.".format(epoch))
                print("Saving checkpoint...")
                save_checkpoint(checkpoint,f"my_checkpoint_{epoch}.pth.tar")
                # check accuracy
                print("Checking accuracy...")
                check_accuracy(val_loader, model, device, epoch)
                # print some examples to a folder
            if epoch % 10 == 0:
                save_predictions_as_imgs(
                    val_loader, model, device, folder="saved_images/"
                )

if __name__ == "__main__" :
    
    # model = UNET(in_channels=1, out_channels=1).to(device)
    # model = AttUNET(in_channels=1, out_channels=1).to(device)
    model = AttUNETa(in_channels=1, out_channels=1).to(device)
    # model = AttUNETb(in_channels=1, out_channels=1).to(device)
    main("my_checkpoint_40.pth.tar",model)

