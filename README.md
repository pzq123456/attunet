# Axial Attention Unet 
> We have four different attention modules in this repository. The best one is Axial Attention Unet(`attUnetb` in code).

## 运行代码
### 1. 安装依赖
```shell
pip install -r requirements.txt
```
### 2. 下载数据集
> - 参照数据集制作教程[link](data/README.md)

### 3. 训练
```shell
python train.py
```

### 4. 预测结果及验证精度
```shell
python check.py
```

